# README #

Web-app for generating PDF documents.

### Install ###

+ run *go build* in a root path
+ run app
+ type in your browser http://localhost:9631/kbgd
+ app require authentication (Login: trivium, Password: trivium)

### Additional dependencies ###

+ github.com/djimenez/iconv-go
+ github.com/jung-kurt/gofpdf

###Tests###
In path deals/kbgd run *go test*

### Fonts ###
If you want to use some other additional fonts, you have to make it first with  [gofpdf](https://github.com/jung-kurt/gofpdf/blob/master/README.md#nonstandard-fonts)
