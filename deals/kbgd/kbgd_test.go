package kbgd

import (
	"testing"
	"fmt"
	"sync"
	"strconv"
	"os"
)
func Test(t *testing.T) {
	  application_path := app_path
	  app_path = "/home/anton/golang/src/pdfgen"
      defer func(application_path string) {
		app_path = application_path
	  }(application_path)
	  if _,err := os.Stat(app_path); err != nil {
		  if os.IsNotExist(err) {
              fmt.Println(err)
			  return
		  }
		  fmt.Printf("Unexpected error - %s",err)
		  return
	  }
	  fill := Fill {
	  Client: "Петров Петр Петрович",
	  DealDate: "02.02.2016",
	  Train: "6346",
	  Category: "1 класс",
	  Wagon: "1",
	  Seats: "1,2",
	  TripDate: "06.02.2016",
	  StartPlace: "Драмтеатра",
	  StartTime: "08:10",
	  Day: "Воскресенье",
	  Phones: "+7 495 555 5555",
	  Salesman: "Иванова Мария Ивановна",
	  Tourists: "Турист #1\nТурист #2\nТурист #3",
	  Adults: "3",
	  Children: "0",
	  Total: "3",
	  Lagguge: "2",
	  Ferry: "3",
	  Bus: "3",
	  Breakfast: "нет",
	  Lunch: "нет",
	  Dinner: "нет",
	  Lunchbox: "нет",
	  Stamp: false,
	  Price: "10000 руб.",
	  FileEngSalesman: "Guseva-Mariya",
	}
	var wg sync.WaitGroup
	for i := 1; i < 11; i++ {
		wg.Add(1)
		go func(i int) {
			fmt.Println(i)
			fill.Adults = strconv.FormatInt(int64(i),10)
			if _,err := createFKBGDTicket(fill); err != nil {
		        t.Errorf("Not generated fill object %+v\n%s",fill,err)
		    }
			wg.Done()
		}(i)
    }
	wg.Wait()
    return
}

func TestGenFileName(t *testing.T) {
	var wg sync.WaitGroup
	for i := 1; i < 31; i++ {
		wg.Add(1)
		go func(i int) {
			str := genFileName("Guseva-Mariya")
			if len(str) < 22 {
		        t.Errorf("Filename was not generated correctly - %s",str)
		    } else {
				fmt.Println(str)
			}
			wg.Done()
		}(i)
    }
	wg.Wait()
    return

}
