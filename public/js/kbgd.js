var kbgdApp = angular.module('kbgdApp',['ui.bootstrap']);

kbgdApp.controller('KbgdCtrl',['$scope','$http','$window','$filter','$modal','$log','$location',
                      function($scope,$http,$window,$filter,$modal,$log,$location){
  var date = new Date();
  $scope.deal_date = date.setDate(date.getDate());
  var tmp = date.getDay() + 1;
  while (tmp == 1 || tmp == 2) {
    tmp++;
  }
  $scope.trip_date = date.setDate(date.getDate() + (tmp-date.getDay()));
  $scope.minDate = new Date();
  $scope.deal_date_opened = false;
  $scope.trip_date_opened = false;
  $scope.adults = 2;
  $scope.children = 0;
  $scope.lagguge = 0;
  $scope.ferry = 2;
  $scope.bus = 2;
  $scope.day = (tmp == 7)?0:tmp;
  $scope.salesman = 0;
  $scope.train = "6346";
  $scope.wagon = "1";
  $scope.seats = "1,2";
  $scope.Meals = {
    breakfast: false,
    lunch: false,
    dinner: false,
    lunchbox: false
  }
  $scope.stamp = false;
  $scope.tourists = "";
  $scope.start_place = 1;
  $scope.client = "";
  $scope.phones = "";
  $scope.price = "";
  $scope.start_time = "08:30";
  $scope.category = 1;
  $scope.dateOptions = {
        'starting-day': 1,
        'showWeeks': false
  };

  $scope.resetFields = function(){
    $scope.clientBg = "white";
    $scope.phonesBg = "white";
    $scope.touristsBg = "white";
  }
  $scope.resetFields();

  $scope.replaceNewLines = function(str){
    str = str.replace(/\n\r/g,", ");
    return str.replace(/\n/g,", ");
  }

   $scope.isFillCorrect = function(){
     var flag = true
     if ($scope.client == ""){
       $scope.clientBg ="red";
       flag = false;
     }
     if ($scope.phones == ""){
       $scope.phonesBg ="red";
       flag = false;
     }
     if ($scope.tourists == ""){
       $scope.touristsBg ="red";
       flag = false;
     }
     return flag;
   }

  $scope.processData = function(){
    $scope.resetFields();
    if ($scope.isFillCorrect()) {
      var kbgd = {"Client":$scope.client,
                  "Train":$scope.train,
                  "DealDate": $filter('date')($scope.deal_date, "dd.MM.yyyy"),
                  "Category":$scope.category,
                  "Wagon":$scope.wagon,
                  "Seats":$scope.seats,
                  "TripDate":$filter('date')($scope.trip_date, "dd.MM.yyyy"),
                  "StartPlace":$scope.start_place,
                  "StartTime":$scope.start_time,
                  "Day":$scope.day,
                  "Phones":$scope.phones,
                  "Salesman":$scope.salesman,
                  "Tourists":$scope.replaceNewLines($scope.tourists),
                  "Children":$scope.children,
                  "Adults":$scope.adults,
                  "Lagguge":$scope.lagguge,
                  "Ferry":$scope.ferry,
                  "Bus":$scope.bus,
                  "Breakfast":($scope.Meals.breakfast) ? "on":"off",
                  "Lunch":($scope.Meals.lunch) ? "on":"off",
                  "Dinner":($scope.Meals.dinner) ? "on":"off",
                  "Lunchbox":($scope.Meals.lunchbox) ? "on":"off",
                  "Price":$scope.price,
                  "Stamp":$scope.stamp
                };
      $scope.sendData(kbgd);
    }
  };

  $scope.sendData = function(kbgd){
    $http({method: 'POST',data: '{'
          +'"Client":"' + kbgd.Client
          +'","DealDate":"' + kbgd.DealDate
          +'","Train":"' + kbgd.Train
          +'","Category":"' + kbgd.Category
          +'","Wagon":"' + kbgd.Wagon
          +'","Seats":"' + kbgd.Seats
          +'","TripDate":"' + kbgd.TripDate
          +'","StartPlace":"' + kbgd.StartPlace
          +'","Day":"' + kbgd.Day
          +'","StartTime":"' + kbgd.StartTime
          +'","Phones":"' + kbgd.Phones
          +'","Salesman":"' + kbgd.Salesman
          +'","Tourists":"' + kbgd.Tourists
          +'","Children":"' + kbgd.Children
          +'","Adults":"' + kbgd.Adults
          +'","Lagguge":"' + kbgd.Lagguge
          +'","Ferry":"' + kbgd.Ferry
          +'","Bus":"' + kbgd.Bus
          +'","Breakfast":"' + kbgd.Breakfast
          +'","Lunch":"' + kbgd.Lunch
          +'","Dinner":"' + kbgd.Dinner
          +'","Lunchbox":"' + kbgd.Lunchbox
          +'","Price":"' + kbgd.Price
          +'","Stamp":' + kbgd.Stamp + '}'
          ,url:'/kbgd/newvaucher',responseType: 'arraybuffer'}).
          success(function(data,status,headers,config){
            if (status == 200) {
              var file = new Blob([data],{type:'application/pdf'});
              var fileURL = URL.createObjectURL(file);
              window.open(fileURL);
            }
          }).
          error(function(data,status,headers,config){
            console.log("Error callback " + status);
          });
  }

  $scope.openCal = function($event) {
    $event.preventDefault();
    $event.stopPropagation();
    if ($event.target.hasAttributes() &&
       ($event.target.getAttribute("id") === 'deal_date_button' || $event.target.getAttribute("id") === 'deal_date_glyph')){
      $scope.deal_date_opened = !$scope.deal_date_opened;
      $scope.trip_date_opened = false;
    } else if ($event.target.hasAttributes() &&
              ($event.target.getAttribute("id") === 'trip_date_button' || $event.target.getAttribute("id") === 'trip_date_glyph')) {
      $scope.trip_date_opened = !$scope.trip_date_opened;
      $scope.deal_date_opened = false;
    }
  };
  $scope.disabled = function(date,mode){
    return (mode === "day" && (date.getDay() === 1 || date.getDay() === 2));
  }

  $scope.configStart = function(source) {
    var day;
    if (source === 'day'){
      day = parseInt($scope.day);
    } else if (source === 'date') {
      day = $scope.trip_date.getDay();
      $scope.day = day;
    }
      switch (day) {
        case 0:
          $scope.start_place = 1;
          $scope.start_time = "08:30";
          $scope.train = "";
        break;
        case 3:
          $scope.start_place = 0;
          $scope.start_time = "08:00";
          $scope.train = "6342";
        break;
        case 4:
          $scope.start_place = 1;
          $scope.start_time = "08:00";
          $scope.train = "";
        break;
        case 5:
          $scope.start_place = 0;
          $scope.start_time = "08:00";
          $scope.train = "6346";
        break;
        case 6:
          $scope.start_place = 0;
          $scope.start_time = "08:00";
          $scope.train = "6342";
        break;
      }
  };

  //$scope.$on('$viewContentLoaded',$scope.configStart("date"));
}]);
